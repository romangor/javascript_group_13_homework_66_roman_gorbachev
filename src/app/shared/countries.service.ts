import { Country } from './country.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Injectable()
export class CountriesService {
  changeCountries = new Subject<Country[]>();
  loadingCountries = new Subject<boolean>();
  countries: Country[] = [];

  constructor(private http: HttpClient) {
  }

  getCountries() {
    return this.countries.slice();
  }

  fetchCountries() {
    this.loadingCountries.next(true);
    this.http.get<{ [key: string]: { alpha3Code: string, name: string } }>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(key => {
          const data = result[key];
          return new Country(data.name, data.alpha3Code);
        })
      }))
      .subscribe(countries => {
        this.countries = countries;
        this.changeCountries.next(this.countries.slice());
        this.loadingCountries.next(false);
      });
  }

  fetchCountry(code: string) {
    return this.http.get<{ name: string, alpha3Code: string, flag: string, region: string, capital: string, population: number }>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${code}`)
      .pipe(map(result => {
        let urlFlag = `http://146.185.154.90:8080/restcountries/data/${code.toLowerCase()}.svg`;
        return new Country(result.name, result.alpha3Code, urlFlag, result.region, result.capital, result.population);
      }));
  }
}
