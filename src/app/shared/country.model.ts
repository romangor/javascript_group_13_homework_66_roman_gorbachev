export class Country {
  constructor(public name: string,
              public code: string,
              public flag?: string,
              public region?: string,
              public capital?: string,
              public population?: number){}
}
