import { Country } from './country.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { CountriesService } from './countries.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country> {
  constructor(private countriesService: CountriesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const countryCode = route.params['id'];
    return this.countriesService.fetchCountry(countryCode);
  }
}
