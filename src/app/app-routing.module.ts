import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountryResolverService } from './shared/country-resolver.service';

const routes: Routes = [
  {
    path: 'country/:id', component: CountryDetailsComponent,
    resolve: {
      country: CountryResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
