import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { HttpClientModule } from '@angular/common/http';
import { CountriesService } from './shared/countries.service';

@NgModule({
  declarations: [
    AppComponent,
    ListCountriesComponent,
    CountryDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [CountriesService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
