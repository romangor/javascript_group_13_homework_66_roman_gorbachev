import { Component, OnDestroy, OnInit } from '@angular/core';
import { CountriesService } from '../shared/countries.service';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.css']
})
export class ListCountriesComponent implements OnInit, OnDestroy {
  countries: Country[] = [];
  loading: boolean = false;
  countriesChangeSubscription!: Subscription;
  countriesLoadingSubscription!: Subscription;

  constructor(private countriesService: CountriesService) {
  }

  ngOnInit() {
    this.countries = this.countriesService.getCountries();
    this.countriesChangeSubscription = this.countriesService.changeCountries.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesLoadingSubscription = this.countriesService.loadingCountries.subscribe(isUpLoading => {
      this.loading = isUpLoading;
    });
    this.countriesService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesLoadingSubscription.unsubscribe()
    this.countriesChangeSubscription.unsubscribe()
  }
}
